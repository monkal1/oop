﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WebAPI.Database;

namespace Tretris
{
    public partial class Form1 : Form
    {
        static HttpClient client = new HttpClient();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
      
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            HttpResponseMessage response =  await client.GetAsync("http://localhost:65403/api/values");
            var text = response.Content.ReadAsStringAsync();
            Console.WriteLine(text.Result);
        }
    }
}
