﻿using System;

namespace WebAPI.FallingObjects.Bombs
{
    public class ExplodeLineCommand : ICommand
    {
        public ExplodeLineCommand(Bomb bombUnderCommand) : base(bombUnderCommand) { }

        public override void Execute()
        {
            Console.WriteLine("Sprogsta bomba");
        }

        public override void Undo()
        {
            Console.WriteLine("Grąžinama eilutė");
        }
    }
}
