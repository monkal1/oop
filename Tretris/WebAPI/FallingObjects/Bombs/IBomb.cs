﻿namespace WebAPI.FallingObjects.Bombs
{
    public interface IBomb
    {
        void ExplodeLine();
        void ExplodeRadius(IIterator iterator);
    }
}
