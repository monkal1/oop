﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.FallingObjects.Figures;

namespace WebAPI.FallingObjects.Bombs
{
    public interface IIterator
    {
        Figure Next();
    }
}
