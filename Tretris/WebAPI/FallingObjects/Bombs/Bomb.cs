﻿using System.Collections.Generic;
using WebAPI.FallingObjects.Figures;

namespace WebAPI.FallingObjects.Bombs
{
    public abstract class Bomb
    {
        private string color;
        private int size;
        private string title;

        private LinkedList<ICommand> commands = new LinkedList<ICommand>();

        public Bomb(string title, string color, int size)
        {
            this.title = title;
            this.color = color;
            this.size = size;
        }

        public virtual string GetName()
        {
            return this.title;
        }

        public virtual string GetColor()
        {
            return this.color;
        }

        public virtual int GetSize()
        {
            return this.size;
        }

        public virtual void ExplodeOneLine()
        {
            System.Console.WriteLine("Explode one line");
        }

        public virtual void ExplodeSingleRadius(IIterator iterator)
        {
            bool notFinished = true;
            
            while(notFinished)
            {
                Figure figure = iterator.Next();
                if (figure != null)
                {
                    System.Console.WriteLine("Sprogdinama prie bombos esanti figūra");
                }
                else
                {
                    System.Console.WriteLine("Daugiau figūrų nėra");
                    notFinished = false;
                }
               
            }
        }

        public virtual void Run(ICommand cmd)
        {
            cmd.Execute();
            commands.AddLast(cmd);

        }

        public virtual void Undo()
        {
            commands.Last.Value.Undo();
            commands.RemoveLast();
        }

    }
}
