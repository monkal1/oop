﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.FallingObjects.Bombs
{
    public class BombNullObject : Bomb
    {
        public BombNullObject(string title, string color, int size) : base(title, color, size)
        {
        }

        public override string GetName()
        {
            return "Null";
        }

        public string GetColor()
        {
            return "Null";
        }

        public int GetSize()
        {
            return 0;
        }

        public void ExplodeOneLine()
        {
        }

        public void ExplodeSingleRadius()
        {
        }

        public void Run(ICommand cmd)
        {
        }

        public void Undo()
        {
        }
    }
}
