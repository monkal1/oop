﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.FallingObjects.Bombs
{
    public interface IVisitor
    {
        void ExplodeCrossLines(LineBomb lineBomb);
        void ExplodeDoubleRadius(RadiusBomb radiusBomb);
    }
}
