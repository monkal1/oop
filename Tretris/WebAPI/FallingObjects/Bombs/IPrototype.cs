﻿using System;
namespace WebAPI.FallingObjects.Bombs
{
    public interface IPrototype
    {
        IPrototype Clone();

        string GetUsername();

        void SetUsername(string username);
    }

}
