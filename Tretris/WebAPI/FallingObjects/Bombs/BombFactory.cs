﻿using WebAPI.Database;

namespace WebAPI.FallingObjects.Bombs

{
    public class BombFactory: FallingObjectsFactory
    {
        public BombFactory() {}
        public Bomb CreateBomb(string type, string title, string color, int size)
        {
            var instance = DatabaseConnection.GetInstance();
            switch (type)
            {
                case "LineBomb":
                    System.Console.WriteLine("Sukurta nauja LineBomb");
                    return new LineBomb(title, color, size);
                case "RadiusBomb":
                    System.Console.WriteLine("Sukurta nauja RadiusBomb");
                    return new RadiusBomb(title, color, size);
                default:
                    return new BombNullObject(title, color, size);
            }
        }
    }
}
