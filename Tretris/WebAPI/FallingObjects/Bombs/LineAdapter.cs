﻿using System;

namespace WebAPI.FallingObjects.Bombs
{
    public class LineAdapter : IBomb
    {
        private Bomb bomb;

        public LineAdapter(Bomb bomb)
        {
            this.bomb = bomb;
        }

        public void ExplodeLine()
        {
            bomb.ExplodeOneLine();
        }

        public void ExplodeRadius(IIterator iterator)
        {
            bomb.ExplodeSingleRadius(iterator);
        }
    }
}
