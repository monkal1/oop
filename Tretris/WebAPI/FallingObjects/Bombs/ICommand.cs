﻿namespace WebAPI.FallingObjects.Bombs
{
    public abstract class ICommand
    {
        protected Bomb bomb = null;
        public ICommand(Bomb bomb)
        {
            this.SetBomb(bomb);
        }

        public Bomb GetBomb()
        {
            return bomb;
        }

        public void SetBomb(Bomb bomb)
        {
            this.bomb = bomb;
        }

        abstract public void Execute();

        abstract public void Undo();

    }
}
