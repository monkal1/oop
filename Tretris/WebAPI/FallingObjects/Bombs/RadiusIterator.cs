﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.FallingObjects.Figures;

namespace WebAPI.FallingObjects.Bombs
{
    public class RadiusIterator : IIterator
    {
        Random random = new Random();
        int count = 0;

        public Figure Next()
        {
            bool found = false;
            while(!found && count < 4)
            {
                bool[] values = new bool[2] { true, false };
                int number = random.Next(0, 2);
                if(values[number])
                {
                    found = true;
                }
                count++;
            }
            if (found)
            {
                return new FigureI("figureI", 5);
            }
            return null;
        }
    }
}
