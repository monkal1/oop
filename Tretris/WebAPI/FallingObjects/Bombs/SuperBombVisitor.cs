﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.FallingObjects.Bombs
{
    public class SuperBombVisitor : IVisitor
    {
        public void ExplodeCrossLines(LineBomb lineBomb)
        {
            Console.WriteLine("Exploded cross lines");
        }

        public void ExplodeDoubleRadius(RadiusBomb radiusBomb)
        {
            Console.WriteLine("Exploded double radius");
        }
    }
}
