﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.FallingObjects.Figures
{
    public abstract class FigureDecorator : IFigure
    {
        protected IFigure figure;
        public FigureDecorator(IFigure newFigure)
        {
            this.figure = newFigure;
        }

        public abstract int GetRotationDegree();

        public abstract string GetColor();
    }
}
