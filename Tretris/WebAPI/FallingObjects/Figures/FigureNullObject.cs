﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.FallingObjects.Figures
{
    public class FigureNullObject : Figure
    {
        public FigureNullObject(string n, int s) : base(n, s)
        {
        }

        public void Rotate()
        {
        }

        public string GetName()
        {
            return "Null";
        }

        public int GetSize()
        {
            return 0;
        }

        public int GetRotationDegree()
        {
            return 0;
        }

        public string GetColor()
        {
            return "Null";
        }

        public void SetFigureDecorator(IFigure newFigureDecorator)
        {
        }
    }
}
