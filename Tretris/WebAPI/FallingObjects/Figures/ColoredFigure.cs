﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.FallingObjects.Figures
{
    public class ColoredFigure: FigureDecorator
    {
        private List<string> colors = new List<string> { "red", "blue", "green", "yellow" };

        public ColoredFigure(IFigure newFigure): base (newFigure)
        {

        }

        public override string GetColor()
        {
            Random rnd = new Random();
            return colors[rnd.Next(0, 3)];
        }

        public override int GetRotationDegree()
        {
            return -1;
        }
    }
}
