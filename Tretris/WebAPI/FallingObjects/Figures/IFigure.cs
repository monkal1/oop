﻿namespace WebAPI.FallingObjects.Figures
{
    public interface IFigure
    {
        int GetRotationDegree();
        string GetColor();
    }
}
