﻿using System.Collections.Generic;
using WebAPI.FallingObjects.Bombs;

namespace WebAPI.FallingObjects.Figures
{
    public class FallingObjectsFacade
    {
        private List<Figure> figures;
        private List<LineBomb> lineBombs;
        private List<RadiusBomb> radiusBombs;


        public FallingObjectsFacade(List<Figure> figures)
        {
            this.figures = figures;
        }

        public FallingObjectsFacade(List<LineBomb> bombs)
        {
            this.lineBombs = bombs;
        }

        public FallingObjectsFacade(List<RadiusBomb> bombs)
        {
            this.radiusBombs = bombs;
        }

        public void RotateFigureAll()
        {
            foreach(Figure f in figures)
            {
                f.Rotate();
            }
        }

        public void ExplodeAllLineBombs()
        {
            foreach(LineBomb b in lineBombs)
            {
                b.ExplodeOneLine();
            }
        }

        public void ExplodeAllRadiusBombs()
        {
            IIterator iterator = new RadiusIterator();
            foreach (RadiusBomb b in radiusBombs)
            {
                b.ExplodeSingleRadius(iterator);
            }
        }

    }
}
