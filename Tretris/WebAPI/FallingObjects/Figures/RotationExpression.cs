﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.FallingObjects.Figures
{
    public abstract class RotationExpression : Expression
    {
        private List<Expression> expressions;

        public abstract void Execute(Figure figure);
    }
}
