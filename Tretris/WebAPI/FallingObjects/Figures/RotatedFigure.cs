﻿using System;
using System.Collections.Generic;

namespace WebAPI.FallingObjects.Figures
{
    public class RotatedFigure : FigureDecorator
    {
        private List<int> degrees = new List<int> { 90, 180, 270, 360 };

        public RotatedFigure(IFigure newFigure): base (newFigure)
        {

        }

        public override string GetColor()
        {
            return null;
        }

        public override int GetRotationDegree()
        {
            Random rnd = new Random();
            return degrees[rnd.Next(0, 3)];
        }
    }
}
