﻿namespace WebAPI.FallingObjects.Figures
{
    public abstract class Figure : IFigure
    {
        private string name { get; set; }
        private int size { get; set; }
        private IFigure figureDecorator;
        public Figure(string n, int s)
        {
            name = n;
            size = s;
        }

        public virtual void Rotate()
        {
            System.Console.WriteLine("As pasisukau");
        }

        public virtual string GetName()
        {
            return this.name;
        }

        public virtual int GetSize()
        {
            return this.size;
        }

        public virtual int GetRotationDegree()
        {
            return figureDecorator.GetRotationDegree();
        }

        public virtual string GetColor()
        {
            return figureDecorator.GetColor();
        }

        public virtual void SetFigureDecorator(IFigure newFigureDecorator)
        {
            this.figureDecorator = newFigureDecorator;
        }
    }
}
