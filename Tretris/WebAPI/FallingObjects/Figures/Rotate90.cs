﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.FallingObjects.Figures
{
    public class Rotate90 : RotationExpression
    {
        public override void Execute(Figure figure)
        {
            Console.WriteLine("Rotated figure 90 degrees");
        }
    }
}
