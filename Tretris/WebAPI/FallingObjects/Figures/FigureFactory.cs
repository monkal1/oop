﻿using WebAPI.Database;

namespace WebAPI.FallingObjects.Figures
{
    public class FigureFactory:FallingObjectsFactory
    {

        public FigureFactory()
        {

        }
        public Figure CreateFigure(char letter, string title, int size)
        {
            var instance = DatabaseConnection.GetInstance();
            switch (letter)
            {
                case 'I':
                    return new FigureI(title, size);
                case 'J':
                    return new FigureJ(title, size);
                case 'L':
                    return new FigureL(title, size);
                case 'O':
                    return new FigureO(title, size);
                case 'S':
                    return new FigureS(title, size);
                case 'T':
                    return new FigureT(title, size);
                case 'Z':
                    return new FigureZ(title, size);
                default:
                    return new FigureNullObject(title, size);
            }
        }
    }
}
