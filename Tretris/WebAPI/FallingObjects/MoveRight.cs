﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.FallingObjects
{
    public class MoveRight : IMoveStrategy
    {
        public void move()
        {
            Console.WriteLine("Object moved to the right");
        }
    }
}
