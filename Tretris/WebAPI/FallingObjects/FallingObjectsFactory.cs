﻿using WebAPI.FallingObjects.Bombs;
using WebAPI.FallingObjects.Figures;

namespace WebAPI.FallingObjects
{
    public abstract class FallingObjectsFactory
    {
        private IMoveStrategy moveStrategy { get; set; }

        public FallingObjectsFactory()
        {
  
        }

        public static FallingObjectsFactory GetFactory(string type)
        {
            switch (type)
            {
                case "Figure":
                    System.Console.WriteLine("Gaunamas figūros factory");
                    return new FigureFactory();
                case "Bomb":
                    System.Console.WriteLine("Gaunamas bombos factory");
                    return new BombFactory();
                default:
                    break;
            }
            return null;
        }

        public void Fall()
        {
            System.Console.WriteLine("Figure fall");
        }
        public void ChangeDirection(char direction)
        {
            switch (direction)
            {
                case 'L':
                    moveStrategy = new MoveLeft();
                    break;
                case 'R':
                    moveStrategy = new MoveRight();
                    break;
                default:
                    break;
            }
            moveStrategy.move();
        }
    }
}
