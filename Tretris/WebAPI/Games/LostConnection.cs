﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public class LostConnection : IState
    {
        private string name;

        public LostConnection()
        {
            this.name = "LostConnection";
        }

        public bool IsAbleToPlay()
        {
            return false;
        }
    }
}
