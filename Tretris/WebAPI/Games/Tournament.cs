﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    class Tournament : IComposite
    {
        private List<IComposite> games = new List<IComposite>();

        public Tournament() { }

        public void add(IComposite composite)
        {
            games.Add(composite);
            Console.WriteLine("Pridėtas naujas žaidimas");
        }

        public void remove(IComposite composite)
        {
            games.Remove(composite);
            Console.WriteLine("Pašalintas žaidimas");
        }
    }
}
