﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public class Flyweight : IFlyweight
    {
        private List<Player> players;
        private Random random;

        public Flyweight()
        {
            players = new List<Player>();
            random = new Random();
        }

        public void AddPlayer(Player player)
        {
            players.Add(player);
        }

        public Player GetPlayer()
        {
            bool found = false;
            Player player = null;
            while (!found)
            {
                int number = random.Next(0, players.Count);
                player = players[number];
                if (player.GetState().IsAbleToPlay())
                {
                    found = true;
                }
            }
            player.ChangeState(new Playing());
            return player;
        }
    }
}
