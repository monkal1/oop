﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public class SmallGrid : Grid
    {
        public override bool IsBigGrid()
        {
            return false;
        }

        public override bool IsSmallGrid()
        {
            return true;
        }
    }
}
