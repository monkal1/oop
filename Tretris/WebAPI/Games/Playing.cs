﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public class Playing : IState
    {
        private string name;

        public Playing()
        {
            this.name = "Playing";
        }

        public bool IsAbleToPlay()
        {
            return false;
        }
    }
}
