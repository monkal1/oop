﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public class Caretaker
    {
        private List<Memento> gridList;

        public Caretaker()
        {
            gridList = new List<Memento>();
        }

        public void Add(Memento grid)
        {
            gridList.Add(grid);
            Console.WriteLine("Added new memento");
        }

        public Memento Get(int index)
        {
            Console.WriteLine("Receiving last grid");
            return gridList[index];
        }

        public int Size()
        {
            return gridList.Count;
        }
    }
}
