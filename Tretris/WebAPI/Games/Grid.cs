﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public abstract class Grid
    {
        private int sizeX;
        private int sizeY;
        private string color;
        private string theme;

        public Grid() { }

        public Grid CreateGridTemplate()
        {
            if (IsBigGrid())
            {
                SetSizeBig();
            }
            if (IsSmallGrid())
            {
                SetSizeSmall();
            }
            SetColor();
            SetTheme();
            return this;  
        }

        public abstract bool IsBigGrid();

        public abstract bool IsSmallGrid();

        public void SetSizeBig()
        {
            this.sizeX = 10;
            this.sizeY = 20;
            Console.WriteLine("Sukuriamas didelis gridas");
        }

        public void SetSizeSmall()
        {
            this.sizeX = 10;
            this.sizeY = 10;
            Console.WriteLine("Sukuriamas mazas gridas");
        }

        public void SetColor()
        {
            this.color = "black";
        }

        public void SetTheme()
        {
            this.theme = "halloween";
        }

    }
}
