﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public interface IComposite
    {
        void add(IComposite composite);
        void remove(IComposite composite);
    }
}
