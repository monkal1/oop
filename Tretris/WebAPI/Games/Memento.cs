﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public class Memento
    {
        private Grid grid;

        public Memento(Grid grid)
        {
            this.grid = grid;
        }

        public Grid GetGrid()
        {
            return grid;
        }

        public void SetGrid(Grid grid)
        {
            this.grid = grid;
        }
    }
}
