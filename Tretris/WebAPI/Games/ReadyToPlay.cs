﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public class ReadyToPlay : IState
    {
        private string name;

        public ReadyToPlay()
        {
            this.name = "ReadyToPlay";
        }

        public bool IsAbleToPlay()
        {
            return true;
        }
    }
}
