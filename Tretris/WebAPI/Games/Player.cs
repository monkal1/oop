﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebAPI.FallingObjects.Bombs;

namespace WebAPI.Games
{
    public class Player : IObserver, IPrototype
    {
        private string username { get; set; }
        private int points { get; set; }
        private bool isPlaying { get; set; }
        private IState state { get; set; }
        private EmojiMediator mediator { get; set; }

        public Player(string username, EmojiMediator mediator)
        {
            this.username = username;
            this.points = 0;
            this.state = new ReadyToPlay();
            this.mediator = mediator;
        }

        public Player(string username)
        {
            this.username = username;
            this.points = 0;
            this.state = new ReadyToPlay();
        }

        public void Update(bool playing)
        {
            this.isPlaying = playing;
            if (this.isPlaying)
            {
                Console.WriteLine("Player " + this.username + " started a new game");
            }
            else
            {
                Console.WriteLine("Player " + this.username + " finished the game");
            }
        }

        public IPrototype Clone()
        {
            System.Console.WriteLine("Klonuojamas useris");
            return (IPrototype)MemberwiseClone();
        }

        public string GetUsername()
        {
            return username;
        }

        public void SetUsername(string username)
        {
            this.username = username;
        }

        public void ChangeState(IState state)
        {
            this.state = state;
        }

        public IState GetState()
        {
            return this.state;
        }

        public void Send(string emoji)
        {
            mediator.SendEmoji(emoji);
        }

    }
}
