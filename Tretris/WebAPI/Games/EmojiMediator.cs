﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public interface EmojiMediator
    {
        void SendEmoji(string emoji);
    }
}
