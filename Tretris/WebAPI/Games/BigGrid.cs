﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public class BigGrid : Grid
    {
        public override bool IsBigGrid()
        {
            return true;
        }

        public override bool IsSmallGrid()
        {
            return false;
        }
    }
}
