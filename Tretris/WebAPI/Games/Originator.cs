﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public class Originator
    {
        private Grid state;

        public void SetMemento(Memento previousState)
        {
            this.state = previousState.GetGrid();
        }

        public Memento CreateMemento()
        {
            return new Memento(state);
        }
    }
}
