﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public class Paused : IState
    {
        private string name;

        public Paused()
        {
            this.name = "Paused";
        }

        public bool IsAbleToPlay()
        {
            return false;
        }
    }
}
