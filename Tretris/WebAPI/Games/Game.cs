﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPI.Games
{
    public class Game : IComposite, EmojiMediator
    {
        private List<IObserver> players;
        private Grid grid;
        private IFlyweight flyweight;
        private List<string> emojis;

        public Game(IFlyweight flyweight)
        {
            this.players = new List<IObserver>();
            this.flyweight = flyweight;
            Console.WriteLine("Game initialized");
        }

        public void AddPlayers()
        {
            Player player1 = flyweight.GetPlayer();
            Player player2 = flyweight.GetPlayer();
            players.Add(player1);
            players.Add(player2);
            Console.WriteLine("Added 2 players: " + player1.GetUsername() + " " + player2.GetUsername());
        }

        public void RemovePlayer(IObserver player)
        {
            players.Remove(player);
        }

        public void NotifyAll(bool isPlaying)
        {
            foreach (IObserver player in players)
            {
                player.Update(isPlaying);
            }
        }

        public void SetGrid(Grid grid)
        {
            this.grid = grid;
        }

        public void add(IComposite composite)
        {
            // throw new NotImplementedException();
        }

        public void remove(IComposite composite)
        {
            // throw new NotImplementedException();
        }

        public void SendEmoji(string emoji)
        {
            Console.WriteLine("Sent " + emoji + " emoji");
        }
    }
}
