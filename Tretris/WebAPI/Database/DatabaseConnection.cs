﻿using System;
using MySql.Data.MySqlClient;


namespace WebAPI.Database
{
    public class DatabaseConnection
    {
        private static DatabaseConnection instance = null;
        private string connectionString = @"server=localhost;userid=root;password=;database=guestbook;SslMode = none";
        private MySqlConnection conn = null;
        private static object threadLock = new object();

        private DatabaseConnection()
        {
            this.ConnectToDatabase();   
        }

        public static DatabaseConnection GetInstance()
        {
            lock (threadLock)
            {
                if (instance == null)
                {
                    instance = new DatabaseConnection();
                }
            }
            Console.WriteLine("Returning instance");
            return instance;
        }

        private void ConnectToDatabase()
        {
            try
            {
                conn = new MySqlConnection(connectionString);
                conn.Open();
                Console.WriteLine("MySQL version : {0}", conn.ServerVersion);

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error: {0}", ex.ToString());

            }
            catch (Exception e)
            {

                Console.WriteLine(e);
            }
            Console.WriteLine("Database initialized");
        }
    }
}
