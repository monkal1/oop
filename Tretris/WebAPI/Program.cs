﻿using System;
using System.Collections.Generic;
using WebAPI.Database;
using WebAPI.FallingObjects;
using WebAPI.FallingObjects.Bombs;
using WebAPI.FallingObjects.Figures;
using WebAPI.Games;
using WebAPI.Loggers;

namespace WebAPI
{
    class Program
    {
        private static AbstractLogger GetChainOfLoggers()
        {

            AbstractLogger errorLogger = new ErrorLogger(AbstractLogger.ERROR);
            AbstractLogger fileLogger = new FileLogger(AbstractLogger.DEBUG);
            AbstractLogger consoleLogger = new ConsoleLogger(AbstractLogger.INFO);

            errorLogger.SetNextLogger(fileLogger);
            fileLogger.SetNextLogger(consoleLogger);

            return errorLogger;
        }

        static void Main(string[] args)
        {
            AbstractLogger loggerChain = GetChainOfLoggers();

            // Singleton
            Console.WriteLine("Singleton šablonas");
            DatabaseConnection.GetInstance();
            Console.WriteLine("-----------------------------------");

            // Factory
            Console.WriteLine("Factory šablonas");
            BombFactory bombFactory = new BombFactory();
            Bomb lineBomb = bombFactory.CreateBomb("LineBomb", "bomba", "red", 2);
            Console.WriteLine("-----------------------------------");

            // Abstract factory
            Console.WriteLine("Abstract Factory šablonas");
            FallingObjectsFactory fallingObjectsFactory = FallingObjectsFactory.GetFactory("Figure");
            Console.WriteLine("-----------------------------------");

            // Strategy
            Console.WriteLine("Strategy šablonas");
            fallingObjectsFactory.ChangeDirection('L');
            Console.WriteLine("-----------------------------------");

            // Observer
            Console.WriteLine("Observer šablonas");
            IFlyweight flyweight = new Flyweight();
            flyweight.AddPlayer(new Player("player1"));
            flyweight.AddPlayer(new Player("player2"));
            flyweight.AddPlayer(new Player("player3"));
            flyweight.AddPlayer(new Player("player4"));
            Game game = new Game(flyweight);
            game.AddPlayers();
            game.NotifyAll(true);
            game.NotifyAll(false);
            Console.WriteLine("-----------------------------------");


            var bomb = new LineBomb("bomb", "black", 1);

            // Adapter
            Console.WriteLine("Adapter šablonas");
            IBomb ibomb = new LineAdapter(bomb);
            ibomb.ExplodeLine();
            Console.WriteLine("-----------------------------------");

            // Prototype
            Console.WriteLine("Prototype šablonas");
            Player player = new Player("Karolina");
            Console.WriteLine("Main user " + player.GetUsername());
            IPrototype playerClone = player.Clone();
            playerClone.SetUsername("");
            Console.WriteLine("Cloned user " + playerClone.GetUsername());
            Console.WriteLine("-----------------------------------");

            // Command
            Console.WriteLine("Command šablonas");
            bomb.Run(new ExplodeLineCommand(bomb));
            bomb.Undo();
            Console.WriteLine("-----------------------------------");

            // Decorator
            Console.WriteLine("Decorator šablonas");
            FigureFactory figureFactory = new FigureFactory();
            Figure figure = figureFactory.CreateFigure('I', "Figura", 1);
            figure.SetFigureDecorator(new RotatedFigure(figureFactory.CreateFigure('I', "Figura", 1)));
            var rotation = figure.GetRotationDegree();
            Console.WriteLine("Figura pasiverte " + rotation + " laipsniu kampu");
            Console.WriteLine("-----------------------------------");

            // Facade
            Console.WriteLine("Facade šablonas");
            List<Figure> figures = new List<Figure>();
            figures.Add(figure);
            FallingObjectsFacade shapeMaker = new FallingObjectsFacade(figures);
            shapeMaker.RotateFigureAll();
            Console.WriteLine("-----------------------------------");

            //------------------------------------------------------------------------------------------

            // TemplateMethod
            Console.WriteLine("Template method");
            Grid grid = new BigGrid();
            grid.CreateGridTemplate();
            game.SetGrid(grid);
            Console.WriteLine("-----------------------------------");

            //------------------------------------------------------------------------------------------

            // NullObject
            Console.WriteLine("Null object pattern");
            var nullBomb = bombFactory.CreateBomb("lala", "vardas", "spalva", 0);
            Console.WriteLine(nullBomb.GetName());
            Console.WriteLine("-----------------------------------");

            //------------------------------------------------------------------------------------------

            // Flywight
            Console.WriteLine("Flyweight");
           
            Game game1 = new Game(flyweight);
            game1.AddPlayers();
            Console.WriteLine("-----------------------------------");

            //------------------------------------------------------------------------------------------

            // Composite
            Console.WriteLine("Composite");
            Tournament tournament = new Tournament();
            tournament.add(game1);
            tournament.add(game);
            tournament.remove(game1);
            Console.WriteLine("-----------------------------------");

            //------------------------------------------------------------------------------------------

            // Iterator
            Console.WriteLine("Iterator");
            IIterator iterator = new RadiusIterator();
            bomb.ExplodeSingleRadius(iterator);
            Console.WriteLine("-----------------------------------");

            //------------------------------------------------------------------------------------------

            // Chain of Responsibility
            Console.WriteLine("Chain of Responsibility");
            loggerChain.LogMessage(AbstractLogger.INFO, "This is an information.");

            loggerChain.LogMessage(AbstractLogger.DEBUG, "This is a debug level information.");

            loggerChain.LogMessage(AbstractLogger.ERROR, "This is an error information.");
            Console.WriteLine("-----------------------------------");

            //------------------------------------------------------------------------------------------

            // Visitor
            Console.WriteLine("Visitor");
            IVisitor visitor = new SuperBombVisitor();
            var radiusBomb = new RadiusBomb("radius", "red", 1);
            visitor.ExplodeCrossLines(bomb);
            visitor.ExplodeDoubleRadius(radiusBomb);
            Console.WriteLine("-----------------------------------");

            //------------------------------------------------------------------------------------------

            // Mediator
            Console.WriteLine("Mediator");
            Player player3 = new Player("Viktorija", game);
            player3.Send("like");
            Console.WriteLine("-----------------------------------");

            //------------------------------------------------------------------------------------------

            // Interpreter
            Console.WriteLine("Interpreter");
            Expression rotate180 = new Rotate180();
            rotate180.Execute(figure);
            Console.WriteLine("-----------------------------------");

            //------------------------------------------------------------------------------------------

            // Memento
            Console.WriteLine("Memento");
            Originator originator = new Originator();
            Memento memento = new Memento(grid);
            originator.SetMemento(memento);
            Memento newMemento = originator.CreateMemento();
            Caretaker caretaker = new Caretaker();
            caretaker.Add(newMemento);
            int size = caretaker.Size();
            Grid lastGrid = caretaker.Get(size - 1).GetGrid();
            Console.WriteLine("-----------------------------------");

            //------------------------------------------------------------------------------------------

            Console.ReadKey();
        }
    }
}
